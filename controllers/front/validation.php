<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author maxiPago!
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class MaxipagoValidationModuleFrontController extends ModuleFrontController
{
    protected $_countryCodes = array(
        'AD' => '376',
        'AE' => '971',
        'AF' => '93',
        'AG' => '1268',
        'AI' => '1264',
        'AL' => '355',
        'AM' => '374',
        'AN' => '599',
        'AO' => '244',
        'AQ' => '672',
        'AR' => '54',
        'AS' => '1684',
        'AT' => '43',
        'AU' => '61',
        'AW' => '297',
        'AZ' => '994',
        'BA' => '387',
        'BB' => '1246',
        'BD' => '880',
        'BE' => '32',
        'BF' => '226',
        'BG' => '359',
        'BH' => '973',
        'BI' => '257',
        'BJ' => '229',
        'BL' => '590',
        'BM' => '1441',
        'BN' => '673',
        'BO' => '591',
        'BR' => '55',
        'BS' => '1242',
        'BT' => '975',
        'BW' => '267',
        'BY' => '375',
        'BZ' => '501',
        'CA' => '1',
        'CC' => '61',
        'CD' => '243',
        'CF' => '236',
        'CG' => '242',
        'CH' => '41',
        'CI' => '225',
        'CK' => '682',
        'CL' => '56',
        'CM' => '237',
        'CN' => '86',
        'CO' => '57',
        'CR' => '506',
        'CU' => '53',
        'CV' => '238',
        'CX' => '61',
        'CY' => '357',
        'CZ' => '420',
        'DE' => '49',
        'DJ' => '253',
        'DK' => '45',
        'DM' => '1767',
        'DO' => '1809',
        'DZ' => '213',
        'EC' => '593',
        'EE' => '372',
        'EG' => '20',
        'ER' => '291',
        'ES' => '34',
        'ET' => '251',
        'FI' => '358',
        'FJ' => '679',
        'FK' => '500',
        'FM' => '691',
        'FO' => '298',
        'FR' => '33',
        'GA' => '241',
        'GB' => '44',
        'GD' => '1473',
        'GE' => '995',
        'GH' => '233',
        'GI' => '350',
        'GL' => '299',
        'GM' => '220',
        'GN' => '224',
        'GQ' => '240',
        'GR' => '30',
        'GT' => '502',
        'GU' => '1671',
        'GW' => '245',
        'GY' => '592',
        'HK' => '852',
        'HN' => '504',
        'HR' => '385',
        'HT' => '509',
        'HU' => '36',
        'ID' => '62',
        'IE' => '353',
        'IL' => '972',
        'IM' => '44',
        'IN' => '91',
        'IQ' => '964',
        'IR' => '98',
        'IS' => '354',
        'IT' => '39',
        'JM' => '1876',
        'JO' => '962',
        'JP' => '81',
        'KE' => '254',
        'KG' => '996',
        'KH' => '855',
        'KI' => '686',
        'KM' => '269',
        'KN' => '1869',
        'KP' => '850',
        'KR' => '82',
        'KW' => '965',
        'KY' => '1345',
        'KZ' => '7',
        'LA' => '856',
        'LB' => '961',
        'LC' => '1758',
        'LI' => '423',
        'LK' => '94',
        'LR' => '231',
        'LS' => '266',
        'LT' => '370',
        'LU' => '352',
        'LV' => '371',
        'LY' => '218',
        'MA' => '212',
        'MC' => '377',
        'MD' => '373',
        'ME' => '382',
        'MF' => '1599',
        'MG' => '261',
        'MH' => '692',
        'MK' => '389',
        'ML' => '223',
        'MM' => '95',
        'MN' => '976',
        'MO' => '853',
        'MP' => '1670',
        'MR' => '222',
        'MS' => '1664',
        'MT' => '356',
        'MU' => '230',
        'MV' => '960',
        'MW' => '265',
        'MX' => '52',
        'MY' => '60',
        'MZ' => '258',
        'NA' => '264',
        'NC' => '687',
        'NE' => '227',
        'NG' => '234',
        'NI' => '505',
        'NL' => '31',
        'NO' => '47',
        'NP' => '977',
        'NR' => '674',
        'NU' => '683',
        'NZ' => '64',
        'OM' => '968',
        'PA' => '507',
        'PE' => '51',
        'PF' => '689',
        'PG' => '675',
        'PH' => '63',
        'PK' => '92',
        'PL' => '48',
        'PM' => '508',
        'PN' => '870',
        'PR' => '1',
        'PT' => '351',
        'PW' => '680',
        'PY' => '595',
        'QA' => '974',
        'RO' => '40',
        'RS' => '381',
        'RU' => '7',
        'RW' => '250',
        'SA' => '966',
        'SB' => '677',
        'SC' => '248',
        'SD' => '249',
        'SE' => '46',
        'SG' => '65',
        'SH' => '290',
        'SI' => '386',
        'SK' => '421',
        'SL' => '232',
        'SM' => '378',
        'SN' => '221',
        'SO' => '252',
        'SR' => '597',
        'ST' => '239',
        'SV' => '503',
        'SY' => '963',
        'SZ' => '268',
        'TC' => '1649',
        'TD' => '235',
        'TG' => '228',
        'TH' => '66',
        'TJ' => '992',
        'TK' => '690',
        'TL' => '670',
        'TM' => '993',
        'TN' => '216',
        'TO' => '676',
        'TR' => '90',
        'TT' => '1868',
        'TV' => '688',
        'TW' => '886',
        'TZ' => '255',
        'UA' => '380',
        'UG' => '256',
        'US' => '1',
        'UY' => '598',
        'UZ' => '998',
        'VA' => '39',
        'VC' => '1784',
        'VE' => '58',
        'VG' => '1284',
        'VI' => '1340',
        'VN' => '84',
        'VU' => '678',
        'WF' => '681',
        'WS' => '685',
        'XK' => '381',
        'YE' => '967',
        'YT' => '262',
        'ZA' => '27',
        'ZM' => '260',
        'ZW' => '263'
    );

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        $cart = $this->context->cart;

        $method = Tools::getValue('method');

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
            Tools::redirect('index.php?controller=order&step=1');

        $customer = new Customer($cart->id_customer);

        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'maxipago') {
                $authorized = true;
                break;
            }
        }

        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $currency = $this->context->currency;

        switch ($method) {
            case 'boleto':
                $displayName = $this->module->displayName . ' - ' . $this->module->l('Boleto Bancário');
                $boletoDiscount = floatval(preg_replace('/[^0-9.]/', '', str_replace(",", ".", Configuration::get('MAXIPAGO_BOLETO_DISCOUNT'))));
                if (floatval($boletoDiscount) > 0) {
                    $amountDiscount = (float)($total * ($boletoDiscount / 100));
                    $cartRule = $this->_createDiscount($cart->id_customer, $amountDiscount, 'Boleto');
                    $cart->addCartRule($cartRule->id);
                    $total = (float)$cart->getOrderTotal(true);
                }
                break;
            case 'card':
                $displayName = $this->module->displayName . ' - ' . $this->module->l('Cartão de Crédito');
                break;
            case 'debit_card':
                $displayName = $this->module->displayName . ' - ' . $this->module->l('Cartão de Débito');
                break;
            case 'tef':
                $displayName = $this->module->displayName . ' - ' . $this->module->l('TEF');
                $tefDiscount = floatval(preg_replace('/[^0-9.]/', '', str_replace(",", ".", Configuration::get('MAXIPAGO_TEF_DISCOUNT'))));
                if (floatval($tefDiscount) > 0) {
                    $amountDiscount = (float)($total * ($tefDiscount / 100));
                    $cartRule = $this->_createDiscount($cart->id_customer, $amountDiscount, 'TEF');
                    $cart->addCartRule($cartRule->id);
                    $total = (float)$cart->getOrderTotal(true);
                }
                break;
            case 'redepay':
                $displayName = $this->module->displayName . ' - ' . $this->module->l('RedePay');
                break;
        }

        $this->module->validateOrder($cart->id, Configuration::get('MAXIPAGO_ORDER_CODE_PENDING'), $total, $this->module->displayName, NULL, NULL, NULL, false, $customer->secure_key);

        switch ($method) {
            case 'boleto':
                $this->_boletoMethod($total);
                break;
            case 'card':
                $this->_cardMethod($total);
                break;
            case 'debit_card':
                $response = $this->_debitCard($total);
                break;
            case 'tef':
                $response = $this->_tefMethod($total);
                break;
            case 'redepay':
                $response = $this->_redepayMethod($total);
                break;
        }

        Tools::clearSmartyCache();

        if($response)
            Tools::redirect($response);
        else
        {
            if(substr(_PS_VERSION_, 0, 3) == '1.7')
                Tools::redirect('index.php?controller=order-detail&id_order=' . $this->module->currentOrder);
            else
                Tools::redirect('index.php?controller=order-confirmation&id_cart=' . $cart->id . '&id_module=' . $this->module->id . '&id_order=' . $this->module->currentOrder . '&key=' . $customer->secure_key);
        }
    }

    /**
     * Send the payment method Boleto to maxiPago!
     */
    protected function _boletoMethod($totalOrder)
    {
        $cart = $this->context->cart;

        //Boleto
        $methodEnabled = Configuration::get('MAXIPAGO_BOLETO_ENABLED');
        if ($methodEnabled) {

            $isSandbox = Configuration::get('MAXIPAGO_SANDBOX');

            $dayToExpire = (int) Configuration::get('MAXIPAGO_BOLETO_DAYS_TO_EXPIRE');
            $instructions = Configuration::get('MAXIPAGO_BOLETO_INSTRUCTIONS');

            $date = new DateTime();
            $date->modify('+' . $dayToExpire . ' days');
            $expirationDate = $date->format('Y-m-d');

            $boletoBank = $isSandbox ? 12 : Configuration::get('MAXIPAGO_BOLETO_BANK');
            $ipAddress = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : null;

            $cpf = Tools::getValue('payment-boleto-cpf');

            $address = $this->_getAddress($cart);
            $customer = new Customer($cart->id_customer);

            $orderId = (string) $this->module->currentOrder;

            $data = array(
                'referenceNum' => $orderId, //Order ID
                'processorID' => $boletoBank, //Bank Number
                'ipAddress' => $ipAddress,
                'chargeTotal' => $totalOrder,
                'customerIdExt' => $cpf,
                'expirationDate' => $expirationDate,
                'number' => $orderId, //Our Number
                'instructions' => $instructions, //Instructions
                'bname' => $customer->firstname . ' ' . $customer->lastname,
                'baddress' => $address['address1'],
                'baddress2' => $address['address2'],
                'bcity' => $address['city'],
                'bstate' => $address['state'],
                'bpostalcode' => $address['postcode'],
                'bcountry' => $address['postcode'],
                'bemail' => $customer->email,
            );

            $data = array_merge($data, $this->getBillingData());

            $this->module->getMaxipago()->boletoSale($data);
            //Log
            $this->module->log(htmlspecialchars($this->module->getMaxipago()->xmlRequest));
            $this->module->log(htmlspecialchars($this->module->getMaxipago()->xmlResponse));

            $response = $this->module->getMaxipago()->response;

            $boletoUrl = isset($response['boletoUrl']) ? $response['boletoUrl'] : null;
            if (!$boletoUrl) {
                $error = isset($response['errorMessage']) ? $response['errorMessage'] : null;
                die($this->module->l($error));
            }
            $this->_saveTransaction('boleto', $data, $response, $boletoUrl);
        }

    }

    private function getFraudCheckData($orderId)
    {
        $fraud_data = array(
            'fraudCheck' => (Configuration::get('MAXIPAGO_CC_FRAUD_CHECK')) ? 'Y' : 'N'
        );

        if($fraud_data['fraudCheck'] == 'N')
            return $fraud_data;

        return array_merge($fraud_data, $this->getFraudProcessorData($orderId));
    }

    private function getFraudProcessorData($orderId)
    {
        $fraudProcessor = Configuration::get('MAXIPAGO_CC_FRAUD_CHECK_PROCESSOR');

        if($fraudProcessor)
        {
            $processor_data = array(
                'fraudProcessorID' => $fraudProcessor,
                'captureOnLowRisk' => 'N',
                'voidOnHighRisk' => 'Y',
                'websiteId' => 'DEFAULT'
            );

            if($processor_data['fraudProcessorID'] == '98')
            {
                $session_id = session_id();
                $processor_data['fraudToken'] = $session_id;
            } else if ($processor_data['fraudProcessorID'] == '99')
            {
                $merchantId = Configuration::get('MAXIPAGO_SELLER_ID');
                $merchantSecret = Configuration::get('MAXIPAGO_SELLER_SECRET');
                $hash = hash_hmac('md5', $merchantId . '*' . $orderId, $merchantSecret);
                $request_data['fraudToken'] = $hash;
            }

            return $processor_data;
        }
    }

    private function getUnknownCreditCardData()
    {
        $credit_card_data = array();

        $number = Tools::getValue('payment-card-number');
        $exp_month = Tools::getValue('payment-card-expiration-month');
        $exp_year = Tools::getValue('payment-card-expiration-year');
        $cvv = Tools::getValue('payment-card-cvv');

        $credit_card_data['number'] = $number;
        $credit_card_data['creditCardNumber'] = $number;
        $credit_card_data['expMonth'] = $exp_month;
        $credit_card_data['expirationMonth'] = $exp_month;
        $credit_card_data['expYear'] = $exp_year;
        $credit_card_data['expirationYear'] = $exp_year;
        $credit_card_data['cvvNumber'] = $cvv;

        return $credit_card_data;
    }

    private function getDebitCardData()
    {
        $credit_card_data = array();

        $number = Tools::getValue('payment-debit-card-number');
        $exp_month = Tools::getValue('payment-debit-card-expiration-month');
        $exp_year = Tools::getValue('payment-debit-card-expiration-year');
        $cvv = Tools::getValue('payment-debit-card-cvv');

        $credit_card_data['number'] = $number;
        $credit_card_data['creditCardNumber'] = $number;
        $credit_card_data['expMonth'] = $exp_month;
        $credit_card_data['expirationMonth'] = $exp_month;
        $credit_card_data['expYear'] = $exp_year;
        $credit_card_data['expirationYear'] = $exp_year;
        $credit_card_data['cvvNumber'] = $cvv;

        return $credit_card_data;
    }

    private function getStoredCreditCardData($stored_card)
    {
        $credit_card_data = array();

        $cvv_number = Tools::getValue('payment-card-cvv-saved');
        $exp_month = Tools::getValue('payment-card-expiration-month-saved');
        $exp_year = Tools::getValue('payment-card-expiration-year-saved');

        $credit_card_data['token'] = $stored_card['token'];
        $credit_card_data['customerId'] = $stored_card['id_customer_maxipago'];
        $credit_card_data['cvvNumber'] = $cvv_number;
        $credit_card_data['expMonth'] = $exp_month;
        $credit_card_data['expirationMonth'] = $exp_month;
        $credit_card_data['expYear'] = $exp_year;
        $credit_card_data['expirationYear'] = $exp_year;

        return $credit_card_data;
    }

    private function getPaymentData($order_total)
    {
        $payment_data = array();

        $currency_code = $this->context->currency->iso_code;

        $installments = Tools::getValue('payment-card-installments');
        $max_without_interest = (int) Configuration::get('MAXIPAGO_CC_MAX_WITHOUT_INTEREST');
        $interest_rate = Configuration::get('MAXIPAGO_CC_INTEREST_RATE');
        $has_interest = 'N';

        if ($interest_rate && $installments > $max_without_interest) {
            $has_interest = 'Y';
            $order_total = $this->module->getTotalByInstallments($order_total, $installments, $interest_rate);
        }

        $payment_data['chargeTotal'] = $order_total;
        $payment_data['currencyCode'] = $currency_code;
        $payment_data['numberOfInstallments'] = $installments;
        $payment_data['chargeInterest'] = $has_interest;

        return $payment_data;
    }

    private function getOrderData()
    {
        $order_data = array();

        $products = $this->context->cart->getProducts();

        $order_data['itemCount'] = count($products);

        foreach($products as $index => $product)
        {
            // Maxipago starts at 1
            $maxipago_index = $index + 1;

            $formated_price_with_taxes = number_format($product['price_wt'], 2, '.', '');
            $formated_total_with_taxes = number_format($product['total_wt'], 2, '.', '');

            $order_data['itemIndex' . $maxipago_index] = $maxipago_index;
            $order_data['itemProductCode' . $maxipago_index] = $product['reference'];
            $order_data['itemDescription' . $maxipago_index] = $product['name'];
            $order_data['itemQuantity' . $maxipago_index] = $product['cart_quantity'];
            $order_data['itemUnitCost' . $maxipago_index] = $formated_price_with_taxes;
            $order_data['itemTotalAmount' . $maxipago_index] = $formated_total_with_taxes;
        }

        return $order_data;
    }

    private function clean_number($number)
    {
        return preg_replace('/\D/', '', $number);
    }

    private function getDocumentInfo()
    {
        $document_number = Tools::getValue('payment-redepay-document');
        $document_number = $this->clean_number($document_number);

        $document = array(
            'owner_type' => 'Individual',
            'document_type' => 'CPF',
            'document_number' => $document_number
        );

        if (strlen($document_number) == '14') {
            $document['owner_type'] = 'Legal entity';
            $document['document_type'] = 'CNPJ';
        }

        return $document;
    }

    private function getPhoneInfo($phone)
    {
        $phone_data = array(
            'type' => 'Mobile',
            'country_code' => '55',
            'area_code' => '',
            'number' => ''
        );

        $country_iso_code = $this->context->country->iso_code ? $this->context->country->iso_code : 'BR';
        $phone_data['country_code'] = $this->_countryCodes[$country_iso_code];

        $phone = $this->clean_number($phone);
        $phone_data['area_code'] = $this->getAreaNumber($phone);
        $phone_data['number'] = $this->getPhoneNumber($phone);

        return $phone_data;
    }

    private function getPhoneNumber($telefone)
    {
        if (strlen($telefone) >= 10) {
            $telefone = preg_replace('/^D/', '', $telefone);
            $telefone = substr($telefone, 2, strlen($telefone) - 2);
        }
        return $telefone;
    }

    private function getAreaNumber($telefone)
    {
        $telefone = preg_replace('/^D/', '', $telefone);
        $telefone = substr($telefone, 0, 2);
        return $telefone;
    }

    private function getBillingData()
    {
        $billing_data = array();

        $customer = new Customer($this->context->cart);
        $address = $this->_getAddress($this->context->cart);

        $billing_data['billingName'] = $customer->firstname . ' ' . $customer->lastname;
        $billing_data['billingAddress'] = $address['address1'];
        $billing_data['billingAddress1'] = $address['address1'];
        $billing_data['billingAddress2'] = $address['address2'];
        $billing_data['billingCity'] = $address['city'];
        $billing_data['billingState'] = $address['state'];
        $billing_data['billingCountry'] = $address['country'];
        $billing_data['billingZip'] = $address['postcode'];
        $billing_data['billingPostalCode'] = $address['postcode'];
        $billing_data['billingPhone'] = $address['phone'];
        $billing_data['billingEmail'] = $customer->email;
        $billing_data['billingBirthDate'] = $customer->birthday;
        $billing_data['billingId'] = $customer->id;
        $billing_data['billingGender'] = $customer->gender ? $customer->gender : 'M';

        if(Tools::getValue('method') == 'redepay') {
            $billing_data['billingState'] = $address['state_iso'];
            $billing_data['billingCountry'] = $this->context->country->iso_code ? $this->context->country->iso_code : 'BR';

            $document = $this->getDocumentInfo();
            $billing_data['billingType'] = $document['owner_type'];
            $billing_data['billingDocumentType'] = $document['document_type'];
            $billing_data['billingDocumentValue'] = $document['document_number'];

            $phone = $this->getPhoneInfo($address['phone']);
            $billing_data['billingPhoneType'] = $phone['type'];
            $billing_data['billingPhoneCountryCode'] = $phone['country_code'];
            $billing_data['billingPhoneAreaCode'] = $phone['area_code'];
            $billing_data['billingPhoneNumber'] = $phone['number'];
        }

        return $billing_data;
    }

    private function getShippingData()
    {
        $shipping_data = array();

        $customer = new Customer($this->context->cart);
        $address = $this->_getAddress($this->context->cart);

        $shipping_data['shippingName'] = $customer->firstname . ' ' . $customer->lastname;
        $shipping_data['shippingAddress'] = $address['address1'];
        $shipping_data['shippingAddress1'] = $address['address1'];
        $shipping_data['shippingAddress2'] = $address['address2'];
        $shipping_data['shippingCity'] = $address['city'];
        $shipping_data['shippingState'] = $address['state'];
        $shipping_data['shippingCountry'] = $address['country'];
        $shipping_data['shippingZip'] = $address['postcode'];
        $shipping_data['shippingPostalCode'] = $address['postcode'];
        $shipping_data['shippingPhone'] = $address['phone'];
        $shipping_data['shippingEmail'] = $customer->email;
        $shipping_data['shippingBirthDate'] = $customer->birthday;
        $shipping_data['shippingId'] = $customer->id;
        $shipping_data['shippingGender'] = $customer->gender ? $customer->gender : 'M';

        if(Tools::getValue('method') == 'redepay') {
            $shipping_data['shippingState'] = $address['state_iso'];
            $shipping_data['shippingCountry'] = $this->context->country->iso_code ? $this->context->country->iso_code : 'BR';

            $document = $this->getDocumentInfo();
            $shipping_data['shippingType'] = $document['owner_type'];
            $shipping_data['shippingDocumentType'] = $document['document_type'];
            $shipping_data['shippingDocumentValue'] = $document['document_number'];

            $phone = $this->getPhoneInfo($address['phone']);
            $shipping_data['shippingPhoneType'] = $phone['type'];
            $shipping_data['shippingPhoneCountryCode'] = $phone['country_code'];
            $shipping_data['shippingPhoneAreaCode'] = $phone['area_code'];
            $shipping_data['shippingPhoneNumber'] = $phone['number'];
        }

        return $shipping_data;
    }

    private function hideSensitiveInformation($xml_request)
    {
        $xml_request = preg_replace('/<number>(.*)<\/number>/m', '<number>*****</number>', $xml_request);
        $xml_request = preg_replace('/<cvvNumber>(.*)<\/cvvNumber>/m', '<cvvNumber>***</cvvNumber>', $xml_request);
        $xml_request = preg_replace('/<token>(.*)<\/token>/m', '<token>***</token>', $xml_request);

        return $xml_request;
    }

    private function saveCreditCard()
    {
        $cart = $this->context->cart;
        $customer = new Customer($cart->id_customer);

        $table = _DB_PREFIX_ . 'maxipago_cc_token';
        $customer_id = pSQL($cart->id_customer);
        $sql = 'SELECT * FROM ' . $table . ' WHERE `id_customer` = \'' . $customer_id. '\'';
        $mpCustomer = Db::getInstance()->getRow($sql);

        if (!$mpCustomer) {
            $customerData = array(
                'customerIdExt' => $cart->id_customer,
                'firstName' => $customer->firstname,
                'lastName' => $customer->lastname
            );

            $this->module->getMaxipago()->addProfile($customerData);
            $this->_saveTransaction('add_profile', $customerData, $this->module->getMaxipago()->response, null, false);

            $mpCustomerId = $this->module->getMaxipago()->getCustomerId();

        } else {
            $mpCustomerId = $mpCustomer['id_customer_maxipago'];
        }

        $address = $this->_getAddress($cart);

        $number = Tools::getValue('payment-card-number');
        $exp_month = Tools::getValue('payment-card-expiration-month');
        $exp_year = Tools::getValue('payment-card-expiration-year');

        $date = new DateTime($exp_year . '-' . $exp_month . '-01');
        $date->modify('+1 month');
        $endDate = $date->format('m/d/Y');

        $ccData = array(
            'customerId' => $mpCustomerId,
            'creditCardNumber' => $number,
            'expirationMonth' => $exp_month,
            'expirationYear' => $exp_year,
            'billingName' => $customer->firstname . ' ' . $customer->lastname,
            'billingAddress1' => $address['address1'],
            'billingAddress2' => $address['address2'],
            'billingCity' => $address['city'],
            'billingState' => $address['state'],
            'billingZip' => $address['postcode'],
            'billingPhone' => $address['phone'],
            'billingEmail' => $customer->email,
            'onFileEndDate' => $endDate,
            'onFilePermissions' => 'ongoing',
        );
        $brand = Tools::getValue('payment-card-brand');

        $this->module->getMaxipago()->addCreditCard($ccData);
        $token = $this->module->getMaxipago()->getToken();
        $this->_saveTransaction('save_card', $ccData, $this->module->getMaxipago()->response, null, false);

        if ($token) {
            $ccEnc = substr($number, 0, 6) . 'XXXXXX' . substr($number, -4, 4);
            $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'maxipago_cc_token` 
                            (`id_customer`, `id_customer_maxipago`, `brand`, `token`, `description`)
                        VALUES
                            ("' . $customer->id . '", "' . $mpCustomerId . '", "' . $brand . '", "' . $token . '", "' . $ccEnc . '" )
                        ';

            if (!Db::getInstance()->Execute($sql)) {
                die('mP! Não foi possível salvar o cartão de crédito.');
            }
        }
    }

    protected function _cardMethod($order_total)
    {
        if(Configuration::get('MAXIPAGO_CC_ENABLED'))
        {
            $request_data = array();

            $saved_card = Tools::getValue('payment-card-saved');

            if($saved_card)
            {
                $table_name = _DB_PREFIX_ . 'maxipago_cc_token';
                $id_customer = pSQL($this->context->cart->id_customer);

                $sql = "SELECT * FROM  $table_name WHERE `id_customer` = '$id_customer' AND `description` = '$saved_card'; ";
                $stored_card = Db::getInstance()->getRow($sql);
                $card_brand = strtoupper($stored_card['brand']);
                $brand_processor = Configuration::get('MAXIPAGO_' . $card_brand . '_PROCESSOR');
                $request_data['processorID'] = $brand_processor;

                $credit_card_data = $this->getStoredCreditCardData($stored_card);
                $request_data = array_merge($request_data, $credit_card_data);
            } else
            {
                $card_brand = Tools::getValue('payment-card-brand');
                $brand_processor = Configuration::get('MAXIPAGO_' . $card_brand . '_PROCESSOR');
                $request_data['processorID'] = $brand_processor;

                $credit_card_data = $this->getUnknownCreditCardData();
                $request_data = array_merge($request_data, $credit_card_data);

                if(Tools::getValue('payment-card-save'))
                    $this->saveCreditCard();
            }

            $order_id = (string) $this->module->currentOrder;
            $request_data['referenceNum'] = $order_id;

            $ip_address = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : null;
            $request_data['ipAddress'] = $ip_address;

            $customer_document = Tools::getValue('payment-card-cpf');
            $request_data['customerIdExt'] = $customer_document;

            $soft_descriptor = Configuration::get('MAXIPAGO_SOFT_DESCRIPTOR');
            if($soft_descriptor)
                $request_data['softDescriptor'] = $soft_descriptor;

            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            if($user_agent)
                $request_data['userAgent'] = $user_agent;

            $fraud_check_data = $this->getFraudCheckData($order_id);
            $request_data = array_merge($request_data, $fraud_check_data);

            $payment_data = $this->getPaymentData($order_total);
            $request_data = array_merge($request_data, $payment_data);

            $order_data = $this->getOrderData();
            $request_data = array_merge($request_data, $order_data);

            $billing_data = $this->getBillingData();
            $request_data = array_merge($request_data, $billing_data);

            $shipping_data = $this->getShippingData();
            $request_data = array_merge($request_data, $shipping_data);

            if(Configuration::get('MAXIPAGO_CC_USE_3DS'))
            {
                $request_data['mpiProcessorID'] = Configuration::get('MAXIPAGO_CC_USE_3DS_MPI_PROCESSOR');
                $request_data['onFailure'] = Configuration::get('MAXIPAGO_CC_USE_3DS_FAIL_ACTION');

                if (Configuration::get('MAXIPAGO_CC_PROCESSING_TYPE') == 'auth') {
                    $this->module->getMaxipago()->authCreditCard3DS($request_data);
                    $this->module->log('MaxiPago->CreditCard->AuthWith3DS');
                } else {
                    $this->module->getMaxipago()->saleCreditCard3DS($request_data);
                    $this->module->log('MaxiPago->CreditCard->SaleWith3DS');
                }
            } else
            {
                if (Configuration::get('MAXIPAGO_CC_PROCESSING_TYPE') == 'auth') {
                    $this->module->getMaxipago()->creditCardAuth($request_data);
                    $this->module->log('MaxiPago->CreditCard->Auth');
                } else {
                    $this->module->getMaxipago()->creditCardSale($request_data);
                    $this->module->log('MaxiPago->CreditCard->Sale');
                }
            }

            $xml_request = $this->hideSensitiveInformation($this->module->getMaxiPago()->xmlRequest);
            $this->module->log(htmlspecialchars($xml_request));

            $xml_response = $this->module->getMaxipago()->xmlResponse;
            $this->module->log(htmlspecialchars($xml_response));

            $response = $this->module->getMaxipago()->response;
            $this->_saveTransaction('card', $request_data, $response);

            if(substr(_PS_VERSION_, 0, 3) == '1.7')
            {
                $order_id = (string) $this->module->currentOrder;
                $status = $this->module->getMaxiPago()->isErrorResponse() ? Configuration::get('MAXIPAGO_ORDER_CODE_CANCELED') :  Configuration::get('MAXIPAGO_ORDER_CODE_PAID');
                $this->module->updateOrderHistory($order_id, $status);
            }
        }
    }

    protected function _debitCard($order_total)
    {
        if(!Configuration::get('MAXIPAGO_DC_ENABLED'))
            die($this->module->l('Método não Liberado!'));

        $request_data = array();

        $card_brand = Tools::getValue('payment-debit-card-brand');
        $brand_processor = Configuration::get('MAXIPAGO_DC_' . $card_brand . '_PROCESSOR');
        $request_data['processorID'] = $brand_processor;

        $debit_card_data = $this->getDebitCardData();
        $request_data = array_merge($request_data, $debit_card_data);

        $order_id = (string) $this->module->currentOrder;
        $request_data['referenceNum'] = $order_id;

        $ip_address = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : null;
        $request_data['ipAddress'] = $ip_address;

        $customer_document = Tools::getValue('payment-debit-card-cpf');
        $request_data['customerIdExt'] = $customer_document;

        $soft_descriptor = Configuration::get('MAXIPAGO_DC_SOFT_DESCRIPTOR');
        if($soft_descriptor)
            $request_data['softDescriptor'] = $soft_descriptor;

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if($user_agent)
            $request_data['userAgent'] = $user_agent;

        $fraud_check_data = $this->getFraudCheckData($order_id);
        $request_data = array_merge($request_data, $fraud_check_data);

        $request_data['chargeTotal'] = $order_total;
        $request_data['currencyCode'] = $this->context->currency->iso_code;
        $request_data['mpiProcessorID'] = Configuration::get('MAXIPAGO_DC_MPI_PROCESSOR');
        $request_data['onFailure'] = Configuration::get('MAXIPAGO_DC_FAIL_ACTION');

        $order_data = $this->getOrderData();
        $request_data = array_merge($request_data, $order_data);

        $billing_data = $this->getBillingData();
        $request_data = array_merge($request_data, $billing_data);

        $shipping_data = $this->getShippingData();
        $request_data = array_merge($request_data, $shipping_data);

        $this->module->getMaxipago()->saledebitCard3DS($request_data);

        $xml_request = $this->hideSensitiveInformation($this->module->getMaxiPago()->xmlRequest);
        $this->module->log('Order ' . $order_id . ' (request): ' . $xml_request);

        $xml_response = $this->module->getMaxipago()->xmlResponse;
        $this->module->log('Order ' . $order_id . ' (response): ' . $xml_response);

        $response = $this->module->getMaxipago()->response;
        $authenticationURL = isset($response['authenticationURL']) ? $response['authenticationURL'] : null;

        if(!$authenticationURL)
        {
            $error = isset($response['errorMessage']) ? $response['errorMessage'] : null;
            die($this->module->l($error));
        }

        $this->_saveTransaction('debit_card', $request_data, $response, $authenticationURL);
        return $authenticationURL;
    }

    /**
     * Send the payment method TEF to maxiPago!
     */
    protected function _tefMethod($totalOrder)
    {
        $response = null;
        $methodEnabled = Configuration::get('MAXIPAGO_TEF_ENABLED');

        if ($methodEnabled) {
            $cart = $this->context->cart;

            $isSandbox = Configuration::get('MAXIPAGO_SANDBOX');

            $tefBank = $isSandbox ? 17 : Tools::getValue('payment-tef-bank');
            $cpf = Tools::getValue('payment-tef-cpf');
            $ipAddress = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : null;

            $address = $this->_getAddress($cart);
            $customer = new Customer($cart->id_customer);

            $data = array(
                'referenceNum' => (string) $this->module->currentOrder, //Order ID
                'processorID' => $tefBank, //Bank Number
                'ipAddress' => $ipAddress,
                'chargeTotal' => $totalOrder,
                'customerIdExt' => $cpf,
                'name' => $customer->firstname . ' ' . $customer->lastname,
                'address' => $address['address1'], //Address 1
                'address2' => $address['address2'], //Address 2
                'city' => $address['city'],
                'state' => $address['state'],
                'postalcode' => $address['postcode'],
                'country' => $address['country'],
                'parametersURL' => 'oid=' . $this->module->currentOrder
            );

            $this->module->getMaxipago()->onlineDebitSale($data);

            //Log
            $this->module->log(htmlspecialchars($this->module->getMaxipago()->xmlRequest));
            $this->module->log(htmlspecialchars($this->module->getMaxipago()->xmlResponse));

            $response = $this->module->getMaxipago()->response;

            $onlineDebitUrl = isset($response['onlineDebitUrl']) ? $response['onlineDebitUrl'] : null;
            if (!$onlineDebitUrl) {
                $error = isset($response['errorMessage']) ? $response['errorMessage'] : null;
                die($this->module->l($error));
            }

            $this->_saveTransaction('tef', $data, $response, $onlineDebitUrl);
            return $onlineDebitUrl;
        }

    }

    protected function _redepayMethod($order_total)
    {
        if(!Configuration::get('MAXIPAGO_REDEPAY_ENABLED'))
            die($this->module->l('Método não Liberado!'));

        $request_data = array();

        $order_id = (string) $this->module->currentOrder;
        $request_data['referenceNum'] = $order_id;

        $brand_processor = '18'; // Fixed
        $request_data['processorID'] = $brand_processor;

        $ip_address = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : null;
        $request_data['ipAddress'] = $ip_address;

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if($user_agent)
            $request_data['userAgent'] = $user_agent;

        $parameter_url = 'type=redepay';
        $request_data['parametersURL'] = $parameter_url;

        $request_data['chargeTotal'] = $order_total;
        $request_data['shippingTotal'] = $this->context->cart->getPackageShippingCost();
        $request_data['currencyCode'] = $this->context->currency->iso_code;

        $order_data = $this->getOrderData();
        $request_data = array_merge($request_data, $order_data);

        $billing_data = $this->getBillingData();
        $request_data = array_merge($request_data, $billing_data);

        $shipping_data = $this->getShippingData();
        $request_data = array_merge($request_data, $shipping_data);

        $this->module->getMaxipago()->redepay($request_data);

        $xml_request = $this->hideSensitiveInformation($this->module->getMaxiPago()->xmlRequest);
        $this->module->log('Order ' . $order_id . ' (request): ' . $xml_request);

        $xml_response = $this->module->getMaxipago()->xmlResponse;
        $this->module->log('Order ' . $order_id . ' (response): ' . $xml_response);

        $response = $this->module->getMaxipago()->response;
        $authenticationURL = isset($response['authenticationURL']) ? $response['authenticationURL'] : null;

        if(!$authenticationURL)
        {
            $error = isset($response['errorMessage']) ? $response['errorMessage'] : null;
            die($this->module->l($error));
        }

        $this->_saveTransaction('redepay', $request_data, $response, $authenticationURL);
        return $authenticationURL;
    }

    protected function _getAddress($cart)
    {
        $invoiceAddress = new Address((int) $cart->id_address_invoice);

        $address2 = $invoiceAddress->address2;
        if (property_exists($invoiceAddress, 'address3')) {
            $address2 .= ' ' . $invoiceAddress->address3;
        }
        if (property_exists($invoiceAddress, 'address4')) {
            $address2 .= ' ' . $invoiceAddress->address4;
        }

        $state = "";
        $state_iso = "";
        if (isset($invoiceAddress->id_state)) {
            $state_object = new State((int) $invoiceAddress->id_state);

            $state = $state_object->name;//State::getNameById($invoiceAddress->id_state);
            $state_iso = $state_object->iso_code;
        }

        return array(
            'address1' => $invoiceAddress->address1,
            'address2' => $address2,
            'state' => $state,
            'state_iso' => $state_iso,
            'city' => $invoiceAddress->city,
            'postcode' => $invoiceAddress->postcode,
            'country' => $invoiceAddress->country,
            'phone' => $invoiceAddress->phone,
        );
    }

    /**
     * Creates a cart rule to discount in Boleto an TEF payment methods
     * @param $id_customer
     * @param $amount
     * @param string $type
     * @return CartRule
     */
    protected function _createDiscount($id_customer, $amount, $type = 'Boleto')
    {
        $cartRule = new CartRule();
        $languages = Language::getLanguages();

        foreach ($languages as $key => $language) {
            $array[$language['id_lang']]= 'Desconto por pagamento à vista (' . $type . ')';
        }

        $cartRule->name = $array;
        $cartRule->description = $this->module->l('Desconto por pagamento à vista (' . $type . ')', 'validation');
        $cartRule->id_customer = $id_customer;
        $cartRule->active = 1;
        $cartRule->date_from = date('Y-m-d 00:00:00');
        $cartRule->date_to = date('Y-m-d h:i:s', mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
        $cartRule->minimum_amount = 1;
        $cartRule->minimum_amount_currency = 1;
        $cartRule->quantity = 1;
        $cartRule->quantity_per_user = 1;
        $cartRule->reduction_tax = 1;
        $cartRule->reduction_amount = floatval($amount);
        $cartRule->add();

        return $cartRule;
    }

    /**
     * Save at the DB the data of the transaction and the Boleto URL when the payment is made with boleto
     *
     * @param $method
     * @param $request
     * @param $return
     * @param null $transactionUrl
     * @param boolean $hasOrder
     */
    protected function _saveTransaction($method, $request, $return, $transactionUrl = null, $hasOrder = true)
    {
        $onlineDebitUrl = null;
        $boletoUrl = null;

        if ($transactionUrl) {
            if ($method == 'tef' || $method == 'debit_card') {
                $onlineDebitUrl = $transactionUrl;
            } else if ($method == 'boleto') {
                $boletoUrl = $transactionUrl;
            }
        }

        if (is_object($request) || is_array($request)) {

            if (isset($request['number'])) {
                $request['number'] = substr($request['number'], 0, 6) . 'XXXXXX' . substr($request['number'], -4, 4);
            }

            if (isset($request['creditCardNumber'])) {
                $request['creditCardNumber'] = substr($request['creditCardNumber'], 0, 6) . 'XXXXXX' . substr($request['creditCardNumber'], -4, 4);
            }

            if (isset($request['cvv'])) {
                $request['cvv'] = 'XXX';
            }

            if (Tools::getValue('payment-card-brand')) {
                $request['brand'] = strtoupper(Tools::getValue('payment-card-brand'));
            }

            $request = json_encode($request);
        }

        $responseMessage = null;
        if (is_object($return) || is_array($return)) {
            $responseMessage = isset($return['responseMessage']) ? $return['responseMessage'] : null;
            $return = json_encode($return);
        }

        $id_order = $this->module->currentOrder;
        if (! $hasOrder) {
            $id_order = 0;
        }

        //Log only if debug is active
        $this->module->log($method);
        $this->module->log($request);
        $this->module->log($return);

        $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'maxipago_transactions` 
                    (`id_order`, `boleto_url`, `online_debit_url`, `method`, `request`, `return`, `response_message`, `created_at`)
                VALUES 
                    (
                        "' . pSQL($id_order) . '", 
                        "' . pSQL($boletoUrl) . '",  
                        "' . pSQL($onlineDebitUrl) . '", 
                        "' . pSQL($method) . '" ,
                        "' . pSQL($request) . '", 
                        "' . pSQL($return) . '", 
                        "' . $responseMessage . '", 
                        "' . date('Y-m-d H:i:s'). '" 
                    );';

        if (! Db::getInstance()->Execute($sql)) {
            die('mP! Não foi possível salvar a transação devido a um erro.');
        }
    }
}
