function allCreditCardFieldsAreValid()
{
    return checkCreditCardNumber()
        && checkCreditCardName()
        && checkCreditCardSecurityCode()
        && checkCreditCardDocument()
        && checkCreditCardExpirationMonth()
        && checkCreditCardExpirationYear()
        && checkCreditCardInstallments();
}

function checkCreditCardNumber()
{
    if(isExistingCreditCard())
        return true;

    return $('#payment-card-number').val() != "";
}

function checkCreditCardName()
{
    if(isExistingCreditCard())
        return true;

    return $('#payment-card-owner').val() != "";
}

function checkCreditCardDocument()
{
    return isValidCPF($('#payment-card-cpf').val());
}

function checkCreditCardExpirationMonth()
{
    if(isExistingCreditCard())
        return $('#payment-card-expiration-month-saved').val() != "";
    else
        return $('#payment-card-expiration-month').val() != "";
}

function checkCreditCardExpirationYear()
{
    if(isExistingCreditCard())
        return $('#payment-card-expiration-year-saved').val() != "";
    else
        return $('#payment-card-expiration-year').val() != ""
}

function checkCreditCardSecurityCode()
{
    if(isExistingCreditCard())
        return $('#payment-card-cvv-saved').val() != "";
    else
        return $('#payment-card-cvv').val() != "";
}

function checkCreditCardInstallments()
{
    return $('#payment-card-installments').val() != "";
}

function isExistingCreditCard()
{
    var can_save_credit_card = eval($('#can-save-credit-card').val());

    if(can_save_credit_card)
        return $('#payment-card-saved').val() != "";

    return false;
}

function checkOrderConfirmCreditCardButton()
{
    if(allCreditCardFieldsAreValid())
        enableOrderButton();
    else
        disableOrderButton();
}

function removeThisCard()
{
    console.log('called');

    var desc = $('#payment-card-saved').val();
    var urlAjaxMaxipago = $('#payment-card-remove-ajax').val();

    if (desc) {
        $('#maxipago-cc-ajax-loader').fadeIn();

        $.ajax({
            url: urlAjaxMaxipago,
            type: "POST",
            cache: false,
            headers: {"cache-control": "no-cache"},
            data: {
                action: 'remove-cc',
                ident: desc
            },
            dataType: "json",
            success: function(result) {
                if (result.success == true) {
                    $('#payment-card-saved option[value="' + desc + '"]').remove();
                    $('#payment-card-saved').val('').change();

                    // If only option 'Select one' left ...
                    if($('#payment-card-saved option').length == 1)
                    {
                        // ... there's no card to select
                        $('#payment-card-saved-selector').remove();
                        $('#can-save-credit-card').val('false');
                        checkOrderConfirmCreditCardButton();
                    }
                }

                $('#maxipago-cc-ajax-loader').fadeOut();
            }
        });
    }
}

jQuery(document).ready(function($){

    $('#payment-card-saved').change(function(e) {
       if($('#payment-card-saved').val() == "")
       {
           $('.saved_card').hide();
           $('.new_card').show();
       } else
       {
           $('.saved_card').show();
           $('.new_card').hide();
       }

        checkOrderConfirmCreditCardButton();
        e.stopPropagation();
    });

    $('#payment-card-number, #payment-card-owner, #payment-card-cvv-saved, #payment-card-cvv, #payment-card-cpf').on('keyup', function(e) {
        checkOrderConfirmCreditCardButton();
        e.stopPropagation();
    });

    $('#payment-card-expiration-month, #payment-card-expiration-month-saved, #payment-card-expiration-year, #payment-card-expiration-year-saved').change(function(e) {
        checkOrderConfirmCreditCardButton();
        e.stopPropagation();
    });

});