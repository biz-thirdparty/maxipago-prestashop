function allDebitCardFieldsAreValid()
{
    return checkDebitCardNumber()
        && checkDebitCardName()
        && checkDebitCardMonth()
        && checkDebitCardYear()
        && checkDebitCardCvv()
        && checkDebitCardCPF();
}

function checkDebitCardNumber()
{
    var card_number = $('#payment-debit-card-number').val();

    return card_number.length == 16;
}

function checkDebitCardName()
{
    return $('#payment-debit-card-name').val() != "";
}

function checkDebitCardMonth()
{
    return $('#payment-debit-card-expiration-month').val() != "";
}

function checkDebitCardYear()
{
    return $('#payment-debit-card-expiration-year').val() != "";
}

function checkDebitCardCvv()
{
    return $('#payment-debit-card-cvv').val() != "";
}

function checkDebitCardCPF()
{
    return isValidCPF($('#payment-debit-card-cpf').val());
}

function checkOrderConfirmDebitCardButton()
{
    if(allDebitCardFieldsAreValid())
        enableOrderButton();
    else
        disableOrderButton();
}

jQuery(document).ready(function($){

    $('#payment-debit-card-cpf, #payment-debit-card-number, #payment-debit-card-name, #payment-debit-card-cvv').on('keyup', function(e) {
        checkOrderConfirmDebitCardButton();
        e.stopPropagation();
    });

    $('#payment-debit-card-expiration-month, #payment-debit-card-expiration-year').on('change', function(e) {
        checkOrderConfirmDebitCardButton();
        e.stopPropagation();
    });

});