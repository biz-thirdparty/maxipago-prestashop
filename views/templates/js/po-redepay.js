function checkRedePayCPF()
{
    return isValidCPF($('#payment-redepay-document').val());
}

function checkOrderConfirmRedepayButton()
{
    if(checkRedePayCPF())
        enableOrderButton();
    else
        disableOrderButton();
}

jQuery(document).ready(function($){

    $('#payment-redepay-document').on('keyup', function(e) {
        checkOrderConfirmRedepayButton();
        e.stopPropagation();
    });
});