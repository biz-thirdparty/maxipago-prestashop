function enableOrderButton()
{
    if($('#conditions-to-approve input[required]').prop('checked'))
        $('#payment-confirmation button').attr('disabled', false);
}

function disableOrderButton()
{
    $('#payment-confirmation button').attr('disabled', true);
}

function isValidCPF(cpf)
{
    cpf = cpf.replace(/[^\d]+/g,'');

    if(cpf == '' || cpf.length != 11)
        return false;

    var resto;
    var soma = 0;

    if (
        cpf == "00000000000"
        || cpf == "11111111111"
        || cpf == "22222222222"
        || cpf == "33333333333"
        || cpf == "44444444444"
        || cpf == "55555555555"
        || cpf == "66666666666"
        || cpf == "77777777777"
        || cpf == "88888888888"
        || cpf == "99999999999"
        || cpf == "12345678909"
    ) {
        return false;
    }

    for (i=1; i<=9; i++)
        soma = soma + parseInt(cpf.substring(i-1, i)) * (11 - i);

    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11))
        resto = 0;

    if (resto != parseInt(cpf.substring(9, 10)) )
        return false;

    soma = 0;
    for (i = 1; i <= 10; i++)
        soma = soma + parseInt(cpf.substring(i-1, i)) * (12 - i);

    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11))
        resto = 0;
    if (resto != parseInt(cpf.substring(10, 11) ) )
        return false;

    return true;
}

$(document).ready(function(){
    $('input[name=payment-option]').change(function(){
        $('#conditions-to-approve input[required]').prop('checked', true);
        disableOrderButton()
    });

    $('.cpf_mask').mask('000.000.000-00', {
        placeholder: '___.___.___-__'
    });

    $('.card_mask').mask('0000000000000000', {
        placeholder: '0000 0000 0000 0000'
    })
});