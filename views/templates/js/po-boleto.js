function checkBoletoCPF()
{
    return isValidCPF($('#payment-boleto-cpf').val());
}

function checkOrderConfirmBoletoButton()
{
    if(checkBoletoCPF())
        enableOrderButton();
    else
        disableOrderButton();
}

jQuery(document).ready(function($){

    $('#payment-boleto-cpf').on('keyup', function(e) {
       checkOrderConfirmBoletoButton();
       e.stopPropagation();
    });
});