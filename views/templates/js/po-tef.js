function checkTefCPF()
{
    return isValidCPF($('#payment-tef-cpf').val());
}

function checkOrderConfirmTefButton()
{
    if(checkTefCPF())
        enableOrderButton();
    else
        disableOrderButton();
}

jQuery(document).ready(function($){

    $('#payment-tef-cpf').on('keyup', function(e) {
        checkOrderConfirmTefButton();
        e.stopPropagation();
    });
});