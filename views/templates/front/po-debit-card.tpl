<form id="debit-card-payment-form" action="{$action}" method="post">
    <input type="hidden" name="method" value="debit_card" />
    <p>
        <label>{l s='Bandeira do Cartão' mod='maxipago'}</label>
        <select name="payment-debit-card-brand">
            {foreach $dc_brands as $brand}
                <option value="{$brand}">{$brand|strtoupper}</option>
            {/foreach}
        </select>
    </p>
    <p>
        <label>{l s='Número do Cartão' mod='maxipago'}</label>
        <input type="text" name="payment-debit-card-number" id="payment-debit-card-number" class="card_mask" required/>
    </p>
    <p>
        <label>{l s='Nome no Cartão' mod='maxipago'}</label>
        <input type="text" name="payment-debit-card-owner" id="payment-debit-card-name" required/>
    </p>
    <p>
        <label>{l s='Validade do Cartão' mod='maxipago'}</label>
        <select name="payment-debit-card-expiration-month" id="payment-debit-card-expiration-month" required>
            <option value="">{l s='Mês' mod='maxipago'}</option>
            {foreach $months as $month}
                <option value="{$month}">{$month}</option>
            {/foreach}
        </select>
        /
        <select name="payment-debit-card-expiration-year" id="payment-debit-card-expiration-year" required>
            <option value="">{l s='Ano' mod='maxipago'}</option>
            {foreach $years as $year}
                <option value="{$year}">{$year}</option>
            {/foreach}
        </select>
    </p>
    <p>
        <label>{l s='Código de Segurança' mod='maxipago'}</label>
        <input type="text" name="payment-debit-card-cvv" id="payment-debit-card-cvv" required/>
    </p>
    <p>
        <label>{l s='CPF' mod='maxipago'}</label>
        <input type="text" name="payment-debit-card-cpf" id="payment-debit-card-cpf" class="cpf_mask" required/>
    </p>
</form>