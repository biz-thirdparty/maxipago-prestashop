<form id="redepay-payment-form" action="{$action}" method="post">
    <input type="hidden" name="method" value="redepay" />
    <p>
        <label>{l s='CPF' mod='maxipago'}</label>
        <input type="text" name="payment-redepay-document" id="payment-redepay-document" class="cpf_mask" required/>
    </p>
</form>