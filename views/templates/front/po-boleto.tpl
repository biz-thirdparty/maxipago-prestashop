<form id="boleto-payment-form" action="{$action}" method="post">
    <input type="hidden" name="method" value="boleto" />
    <p>
        <label>{l s='CPF' mod='maxipago'}</label>
        <input type="text" id="payment-boleto-cpf" name="payment-boleto-cpf" class="cpf_mask" required/>
    </p>
</form>