<form id="tef-payment-form" action="{$action}" method="post">
    <input type="hidden" name="method" value="tef" />
    <p>
        <label>{l s='CPF' mod='maxipago'}</label>
        <input type="text" name="payment-tef-cpf" id="payment-tef-cpf" class="cpf_mask" required/>
    </p>
</form>