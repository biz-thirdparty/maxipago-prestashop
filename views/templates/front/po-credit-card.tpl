<form id="credit-card-payment-form" action="{$action}" method="post">
    <input type="hidden" name="method" value="card" />
    <input type="hidden" id="can-save-credit-card" value="{if $cc_can_save}true{else}false{/if}" />
    <input type="hidden" id="payment-card-remove-ajax" value="{$urlAjaxMaxipago}" />
    {if $cc_can_save && !empty($saved_cards)}
    <p id="payment-card-saved-selector">
        <label>{l s='Utilizar Cartão Salvo' mod='maxipago'}</label>
        <select name="payment-card-saved" id="payment-card-saved">
            <option value="">{l s='Selecione' mod='maxipago'}</option>
            {foreach $saved_cards as $card}
                <option value="{$card.description}">{$card.brand|ucwords} - {$card.description}</option>
            {/foreach}
        </select>
        <button onclick="removeThisCard();" type="button" class="btn btn-sm btn-danger saved_card" style="display:none; float:right;">
            Remover este cartão
        </button>
    </p>
    <p class="saved_card" style="display: none;">
        <label>{l s='Validade do Cartão' mod='maxipago'}</label>
        <select name="payment-card-expiration-month-saved" id="payment-card-expiration-month-saved" required>
            <option value="">{l s='Mês' mod='maxipago'}</option>
            {foreach $months as $month}
                <option value="{$month}">{$month}</option>
            {/foreach}
        </select>
        /
        <select name="payment-card-expiration-year-saved" id="payment-card-expiration-year-saved" required>
            <option value="">{l s='Ano' mod='maxipago'}</option>
            {foreach $years as $year}
                <option value="{$year}">{$year}</option>
            {/foreach}
        </select>
    </p>
    <p class="saved_card" style="display: none;">
        <label>{l s='Código de Segurança' mod='maxipago'}</label>
        <input type="text" name="payment-card-cvv-saved" id="payment-card-cvv-saved" required/>
    </p>
    {/if}
    <p class="new_card">
        <label>{l s='Bandeira do Cartão' mod='maxipago'}</label>
        <select name="payment-card-brand" id="payment-card-brand">
            {foreach $cc_brands as $brand}
                <option value="{$brand}">{$brand|strtoupper}</option>
            {/foreach}
        </select>
    </p>
    <p class="new_card">
        <label>{l s='Número do Cartão' mod='maxipago'}</label>
        <input type="text" name="payment-card-number" id="payment-card-number" class="card_mask" required/>
    </p>
    <p class="new_card">
        <label>{l s='Nome no Cartão' mod='maxipago'}</label>
        <input type="text" name="payment-card-owner" id="payment-card-owner" required/>
    </p>
    <p class="new_card">
        <label>{l s='Validade do Cartão' mod='maxipago'}</label>
        <select name="payment-card-expiration-month" id="payment-card-expiration-month" required>
            <option value="">{l s='Mês' mod='maxipago'}</option>
            {foreach $months as $month}
                <option value="{$month}">{$month}</option>
            {/foreach}
        </select>
        /
        <select name="payment-card-expiration-year" id="payment-card-expiration-year" required>
            <option value="">{l s='Ano' mod='maxipago'}</option>
            {foreach $years as $year}
                <option value="{$year}">{$year}</option>
            {/foreach}
        </select>
    </p>
    <p class="new_card">
        <label>{l s='Código de Segurança' mod='maxipago'}</label>
        <input type="text" name="payment-card-cvv" id="payment-card-cvv" required/>
    </p>
    <p>
        <label>{l s='Quantidade de Parcelas' mod='maxipago'}</label>
        <select name="payment-card-installments" id="payment-card-installments" required>
            <option value="">{l s='Selecione' mod='maxipago'}</option>
            {foreach $installments as $installment}
                <option value="{$installment.installments}">
                    {$installment.installments} x {l s='de' mod='maxipago'} {Tools::displayPrice($installment.installment_value)}

                    {if !$installment.interest_rate}
                        ({l s='sem juros' mod='maxipago'})
                    {else}
                        ({$installment.interest_rate}% {l s='a.m.' mod='maxipago'} -
                        {l s='Total' mod='maxipago'}: {Tools::displayPrice($installment.total)})
                    {/if}
                </option>
            {/foreach}
        </select>
    </p>
    <p>
        <label>{l s='CPF' mod='maxipago'}</label>
        <input type="text" name="payment-card-cpf" id="payment-card-cpf" class="cpf_mask" required/>
    </p>
    {if $cc_can_save}
        <p class="new_card">
            <label>
                <input type="checkbox" name="payment-card-save" id="payment-card-save">
                {l s='Salvar Cartão' mod='maxipago'}
            </label>
        </p>
    {/if}
</form>
<div id="maxipago-cc-ajax-loader" style="display: none; position: fixed; width: 100%; height: 100%; left: 0; top: 0; background: rgba(51,51,51,0.7); z-index: 10;">
    <span style="color: white;">{l s='Aguarde ...' mod='maxipago'}</span>
</div>